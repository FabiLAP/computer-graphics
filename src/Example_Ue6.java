import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import lenz.opengl.AbstractSimpleBase;
import lenz.opengl.utils.ShaderProgram;
import lenz.opengl.utils.Texture;

public class Example_Ue6 extends AbstractSimpleBase {
	
	int i =0;
	long time = Sys.getTime();
	long step=0;
	double cubescale = 0;
	Texture mosaic;
	Texture water;
	Texture cubePic;
	ShaderProgram kachelShader;
	ShaderProgram phongShader;
	ShaderProgram gorouadShaderForColor;
	ShaderProgram textureGouroud;
	ShaderProgram phongShaderLong;
	ShaderProgram gorouadShaderMatrixTranslation;
	Matrix4f projection;
	
	public static void main(String[] args) {
		new Example_Ue6().start();
	}

	@Override
	protected void initOpenGL() {
		
		glMatrixMode(GL_PROJECTION);
		
//		glFrustum(-1, 1, -1, 1, 1, 100);
//		double left, double right, 
//		double bottom, double top, 
//		double zNear, double zFar)
		projection = new Matrix4f();
		projection.m20 = 0;
		projection.m21 = 0;
		projection.m22 = -1*101/99f;
		projection.m23 = -1;
		projection.m32 = -1*200/99f;
		projection.m33 = 0;		
		
		glMatrixMode(GL_MODELVIEW);
		glShadeModel(GL_FLAT);
		
		//schalten fl�chen hinten usw aus		
		glEnable(GL_CULL_FACE);
		
		mosaic = new Texture("/ref/mosaic.jpg", 4);
		water  = new Texture("/ref/wasser08.jpg", 4);
		
		kachelShader = new ShaderProgram("kachelProzedur");
		phongShader = new ShaderProgram("phongShader");
		gorouadShaderForColor = new ShaderProgram("gorouadShaderForColor");
		textureGouroud = new ShaderProgram("textureGouroud");
		phongShaderLong = new ShaderProgram("myPhongTest");
		gorouadShaderMatrixTranslation = new ShaderProgram("gorouadShaderMatrixTranslation");
	}

	@Override
	protected void render() {					
		FloatBuffer projectionBuffer = BufferUtils.createFloatBuffer(16);
		projection.store(projectionBuffer);
		projectionBuffer.flip();
		glUniformMatrix4(glGetUniformLocation(gorouadShaderMatrixTranslation.getId(), "projectionMatrix"),false, projectionBuffer);		
		
		long diffTime = Sys.getTime()-time;
		time = Sys.getTime();
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
		//########################################################################## CUBE RECHTS OBEN
		//########################################################################## textur / gorouad
		//TODO CUBE RECHTS OBEN - textur / gorouad
		GL20.glUseProgram(gorouadShaderMatrixTranslation.getId());
		Matrix4f mvp = new Matrix4f(projection);	 
		mvp.translate(new Vector3f(2,2,-5));
		mvp.rotate(step/1000f, new Vector3f(0, 1, 0));
		
		FloatBuffer mvpBuffer = BufferUtils.createFloatBuffer(16);
		mvp.store(mvpBuffer);
		mvpBuffer.flip();
		glUniformMatrix4(
		glGetUniformLocation(gorouadShaderMatrixTranslation.getId(), "mvpMatrix"),
		false, mvpBuffer);
		
		glEnable(GL_TEXTURE_2D);			
		glBindTexture(GL_TEXTURE_2D, mosaic.getId());

		glBegin(GL_QUADS);		
		//vorn
		glColor3f(1,1,1);
		glNormal3f(0, 0, 1);
		glTexCoord2d(0, 0);		
		glVertex3f(-1,-1,1);		
		glTexCoord2d(1, 0);
		glVertex3f(1,-1,1);		
		glTexCoord2d(1, 1);
		glVertex3f(1,1,1);		
		glTexCoord2d(0, 1);
		glVertex3f(-1,1,1);
				
		//hinten
		glNormal3f(0, 0, -1);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,-1);		
		glTexCoord2d(1, 0);	
		glVertex3f(-1,-1,-1);		
		glTexCoord2d(1, 1);	
		glVertex3f(-1,1,-1);		
		glTexCoord2d(0, 1);	
		glVertex3f(1,1,-1);
				
		//rechts
		glNormal3f(1, 0, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(1,-1,-1);
		glTexCoord2d(1, 1);	
		glVertex3f(1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(1,1,1);
		
		//links
		glNormal3f(-1, 0, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(-1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(-1,1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(-1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(-1,-1,-1);
		
		//oben
		glNormal3f(0, 1, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(-1,1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(1,1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(-1,1,-1);
		glEnd();

		glBegin(GL_QUADS);
		//unten
		glNormal3f(0, -1, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(-1,-1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(-1,-1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(1,-1,-1);
		glEnd();
		
		//########################################################################## CUBE LINKS OBEN
		//########################################################################## textur / phong
		//TODO CUBE LINKS OBEN - textur / phong
		GL20.glUseProgram(phongShader.getId());
		int location = GL20.glGetUniformLocation(phongShader.getId(), "licht");
		GL20.glUniform3f(location, 2, 2, 1);

		mvp = new Matrix4f(projection);	 
		mvp.translate(new Vector3f(-2,2,-5));
		mvp.rotate(step/1000f, new Vector3f(0, 1, 0));
		
		mvpBuffer = BufferUtils.createFloatBuffer(16);
		mvp.store(mvpBuffer);
		mvpBuffer.flip();
		glUniformMatrix4(glGetUniformLocation(phongShader.getId(), "mvpMatrix"),false, mvpBuffer);	
		
		glEnable(GL_TEXTURE_2D);			
		glBindTexture(GL_TEXTURE_2D, mosaic.getId());

		glBegin(GL_QUADS);		
		//vorn
		glColor3f(1,1,1);
		glNormal3f(0, 0, 1);
		glTexCoord2d(0, 0);		
		glVertex3f(-1,-1,1);		
		glTexCoord2d(1, 0);
		glVertex3f(1,-1,1);		
		glTexCoord2d(1, 1);
		glVertex3f(1,1,1);		
		glTexCoord2d(0, 1);
		glVertex3f(-1,1,1);
				
		//hinten
		glNormal3f(0, 0, -1);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,-1);		
		glTexCoord2d(1, 0);	
		glVertex3f(-1,-1,-1);		
		glTexCoord2d(1, 1);	
		glVertex3f(-1,1,-1);		
		glTexCoord2d(0, 1);	
		glVertex3f(1,1,-1);
				
		//rechts
		glNormal3f(1, 0, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(1,-1,-1);
		glTexCoord2d(1, 1);	
		glVertex3f(1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(1,1,1);
		
		//links
		glNormal3f(-1, 0, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(-1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(-1,1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(-1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(-1,-1,-1);
		
		//oben
		glNormal3f(0, 1, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(-1,1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(1,1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(-1,1,-1);
		glEnd();

		glBegin(GL_QUADS);
		//unten
		glNormal3f(0, -1, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(-1,-1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(-1,-1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(1,-1,-1);
		glEnd();
		
		//########################################################################## CUBE LINKS UNTEN 
		//########################################################################## prozedurale textur / gorouad
		//TODO CUBE LINKS UNTEN - prozedurale textur / gorouad
		GL20.glUseProgram(kachelShader.getId());
		location = GL20.glGetUniformLocation(kachelShader.getId(), "licht");
		GL20.glUniform3f(location, 2, 2, 1);

		mvp = new Matrix4f(projection);	 
		mvp.translate(new Vector3f(-3,-2,-5));
		mvp.rotate(step/1000f, new Vector3f(0, 1, 0));
		
		mvpBuffer = BufferUtils.createFloatBuffer(16);
		mvp.store(mvpBuffer);
		mvpBuffer.flip();
		glUniformMatrix4(glGetUniformLocation(kachelShader.getId(), "mvpMatrix"),false, mvpBuffer);
		
		glEnable(GL_TEXTURE_2D);			
		glBindTexture(GL_TEXTURE_2D, mosaic.getId());

		glBegin(GL_QUADS);		
		//vorn
		glColor3f(1,1,1);
		glNormal3f(0, 0, 1);
		glTexCoord2d(0, 0);		
		glVertex3f(-1,-1,1);		
		glTexCoord2d(1, 0);
		glVertex3f(1,-1,1);		
		glTexCoord2d(1, 1);
		glVertex3f(1,1,1);		
		glTexCoord2d(0, 1);
		glVertex3f(-1,1,1);
				
		//hinten
		glNormal3f(0, 0, -1);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,-1);		
		glTexCoord2d(1, 0);	
		glVertex3f(-1,-1,-1);		
		glTexCoord2d(1, 1);	
		glVertex3f(-1,1,-1);		
		glTexCoord2d(0, 1);	
		glVertex3f(1,1,-1);
				
		//rechts
		glNormal3f(1, 0, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(1,-1,-1);
		glTexCoord2d(1, 1);	
		glVertex3f(1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(1,1,1);
		
		//links
		glNormal3f(-1, 0, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(-1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(-1,1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(-1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(-1,-1,-1);
		
		//oben
		glNormal3f(0, 1, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(-1,1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(1,1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(-1,1,-1);
		glEnd();

		glBegin(GL_QUADS);
		//unten
		glNormal3f(0, -1, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(-1,-1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(-1,-1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(1,-1,-1);
		glEnd();

		//########################################################################## Pyramide Rechts Unten
		//########################################################################## color / gorouad
		//TODO Pyramide Rechts Unten - color / gorouad
		GL20.glUseProgram(gorouadShaderForColor.getId());
		mvp = new Matrix4f(projection);
		mvp.translate(new Vector3f(2, -2, -5));
		mvp.rotate(step/700f, new Vector3f(0, 1, 0));

		mvpBuffer = BufferUtils.createFloatBuffer(16);
		mvp.store(mvpBuffer);
		mvpBuffer.flip();
		glUniformMatrix4(
		glGetUniformLocation(gorouadShaderForColor.getId(), "mvpMatrix"), false, mvpBuffer);
		
		//unten
		glBegin(GL_QUADS);		
		glNormal3f(0, -1, 0);
		glColor3f(0.4f,0.4f,0.4f);
		glVertex3f(1,-1,1);
		glVertex3f(-1,-1,1);
		glVertex3f(-1,-1,-1);
		glVertex3f(1,-1,-1);
		glEnd();
		
		//vorn
		glBegin(GL_TRIANGLES);
		glNormal3f(0, 0, 1);
		glColor3f(0.9f,0.9f,0.9f);
		glVertex3f(-1,-1,1);
		glVertex3f(1,-1,1);
		glVertex3f(0,1,0);
		glEnd();
		
		//rechts
		glBegin(GL_TRIANGLES);
		glNormal3f(1, 0, 0);
		glColor3f(0.7f,0.7f,0.7f);
		glVertex3f(1,-1,1);
		glVertex3f(1,-1,-1);
		glVertex3f(0,1,0);
		glEnd();
		
		//hinten
		glBegin(GL_TRIANGLES);
		glNormal3f(0, 0, 1);
		glColor3f(0.8f,0.8f,0.8f);
		glVertex3f(1,-1,-1);
		glVertex3f(-1,-1,-1);
		glVertex3f(0,1,0);
		glEnd();
		
		//links
		glBegin(GL_TRIANGLES);
		glNormal3f(-1, 0, 0);
		glColor3f(0.6f,0.6f,0.6f);
		glVertex3f(-1,-1,-1);
		glVertex3f(-1,-1,1);
		glVertex3f(0,1,0);
		glEnd();
				
		i++;		
		step+=diffTime;		
		cubescale=Math.abs(Math.sin(step/200f)*1.2)+0.2;
	}
}
