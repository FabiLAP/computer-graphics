import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.Display;

import lenz.opengl.AbstractSimpleBase;

public class Example extends AbstractSimpleBase {
	
	int i =0;

	public static void main(String[] args) {
		new Example().start();
	}

	@Override
	protected void initOpenGL() {
		glMatrixMode(GL_PROJECTION);
		glOrtho(0, 800, 0, 600, 0, 1);
		glMatrixMode(GL_MODELVIEW);

		glShadeModel(GL_FLAT);
	}

	@Override
	protected void render() {
		glClear(GL_COLOR_BUFFER_BIT);
		glLoadIdentity();

		glBegin(GL_POINTS);
		glColor3f(1, 0.5f, 0);
		glVertex2i(400, 300); //definiert eine Ecke/Punkt //2i - Anzahl Parameter + Type
		glVertex2i(410, 300);
		glVertex2i(400, 310);
		glVertex2i(410, 310);
		glVertex2i(200, 200);
		glVertex2i(200, 201);
		glVertex2i(200, 202);
		glVertex2i(200, 203);
		glVertex2i(200, 204);
		glEnd();
		
		glBegin(GL_LINES);
		glColor3f(1, 1, 1);
		glVertex2i(100, 100);
		glVertex2i(150, 288);
		glEnd();
		
		
		
		glTranslated(309,309.,0.);
		glRotated(i, 0, 0, 1);		
		glBegin(GL_TRIANGLES);
		glColor3f(1, 0, 1);				
		glVertex2i(0, 0);
		glVertex2i(100, 100);
		glVertex2i(0, 100);		
		glEnd();
		
		glLoadIdentity();

		glTranslated(10+i,200.,0.);
		glBegin(GL_POLYGON);
		glColor3f(0, 1, 1);
		glVertex2i(100, 300); //definiert eine Ecke/Punkt //2i - Anzahl Parameter + Type
		glVertex2i(100, 359);
		glVertex2i(120, 360);
		glVertex2i(110, 380);
		glVertex2i(170, 200);
		glVertex2i(110, 220);
		glVertex2i(145, 131);	
//		glTranslated(200.,200.,200.);
		glEnd();
		
		glLoadIdentity();
		
		
		glScaled(i, i, 0);
		glBegin(GL_TRIANGLES);
		glColor3f(1, 1, 1);				
		glVertex2i(0, 0);
		glVertex2i(1, 1);
		glVertex2i(0, 1);		
		glEnd();

		i++;
		

		
		
	}
}
