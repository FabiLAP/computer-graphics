import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL20;

import lenz.opengl.AbstractSimpleBase;
import lenz.opengl.utils.ShaderProgram;
import lenz.opengl.utils.Texture;

public class Example_Ue5 extends AbstractSimpleBase {
	
	int i =0;
	long time = Sys.getTime();
	long step=0;
	double cubescale = 0;
	Texture mosaic;
	Texture water;
	Texture cubePic;
	ShaderProgram shader;

	public static void main(String[] args) {
		new Example_Ue5().start();
	}

	@Override
	protected void initOpenGL() {
		glMatrixMode(GL_PROJECTION);
		glFrustum(-1, 1, -1, 1, 1, 100);
		glMatrixMode(GL_MODELVIEW);
		glShadeModel(GL_FLAT);
		
		//schalten fl�chen hinten uswe waus
		
		glEnable(GL_CULL_FACE);
		
		mosaic = new Texture("/ref/mosaic.jpg", 4);
		water  = new Texture("/ref/wasser08.jpg", 4);
//		cubePic = new Texture("/ref/TextureUV.jpg", 4);
		
		shader = new ShaderProgram("myShader4");
		GL20.glUseProgram(shader.getId());
		
		int location = GL20.glGetUniformLocation(shader.getId(), "licht");
		GL20.glUniform3f(location, 1, 1, 1);
	}

	@Override
	protected void render() {
		
		long diffTime = Sys.getTime()-time;
		time = Sys.getTime();
			
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		
	
		
		//################################### cube
		
		glEnable(GL_TEXTURE_2D);	
		
		glBindTexture(GL_TEXTURE_2D, mosaic.getId());
//		int location = glGetUn
//		GL20.glUniform3f(glGetUniformLocation("licht"),1,1,1);
//		GL20.glUniform3f(location, v0, v1, v2);
		
		glLoadIdentity();
		glTranslated(0,2,-5);
//		glScaled(cubescale, cubescale, cubescale);
		glRotated(step/10f, 1, 1, 0);
		glBegin(GL_QUADS);
		
		//vorn
		glColor3f(1,1,1);
		glNormal3f(0, 0, 1);
		glTexCoord2d(0, 0);		
		glVertex3f(-1,-1,1);
		
		glTexCoord2d(1, 0);
		glVertex3f(1,-1,1);
		
		glTexCoord2d(1, 1);
		glVertex3f(1,1,1);
		
		glTexCoord2d(0, 1);
		glVertex3f(-1,1,1);
		
		
		
		//hinten
		glNormal3f(0, 0, -1);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,-1);		
		glTexCoord2d(1, 0);	
		glVertex3f(-1,-1,-1);		
		glTexCoord2d(1, 1);	
		glVertex3f(-1,1,-1);		
		glTexCoord2d(0, 1);	
		glVertex3f(1,1,-1);
		
		
		//rechts
		glNormal3f(1, 0, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(1,-1,-1);
		glTexCoord2d(1, 1);	
		glVertex3f(1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(1,1,1);
		
		//links
		glNormal3f(-1, 0, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(-1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(-1,1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(-1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(-1,-1,-1);
		
		
		//oben
		glNormal3f(0, 1, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(-1,1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(1,1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(1,1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(-1,1,-1);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, water.getId());
		glBegin(GL_QUADS);
		//unten
		glNormal3f(0, -1, 0);
		glTexCoord2d(0, 0);	
		glVertex3f(1,-1,1);
		glTexCoord2d(1, 0);	
		glVertex3f(-1,-1,1);
		glTexCoord2d(1, 1);	
		glVertex3f(-1,-1,-1);
		glTexCoord2d(0, 1);	
		glVertex3f(1,-1,-1);
		glEnd();
		
		//###############################################
		
//		glLoadIdentity();
//		glTranslated(0,-2,-5);
//		glRotated(step/5f, 1, 0, 0);
//		glRotated(step/5f, 0, 1, 0);
//		glBegin(GL_QUADS);		
//		
//		//unten
//		glColor3f(0.4f,0.4f,0.4f);
//		glVertex3f(1,-1,1);
//		glVertex3f(-1,-1,1);
//		glVertex3f(-1,-1,-1);
//		glVertex3f(1,-1,-1);
//		glEnd();
//		
//		//vorn
//		glBegin(GL_TRIANGLES);
//		glColor3f(0.9f,0.9f,0.9f);
//		glVertex3f(-1,-1,1);
//		glVertex3f(1,-1,1);
//		glVertex3f(0,1,0);
//		glEnd();
//		
//		//rechts
//		glBegin(GL_TRIANGLES);
//		glColor3f(0.7f,0.7f,0.7f);
//		glVertex3f(1,-1,1);
//		glVertex3f(1,-1,-1);
//		glVertex3f(0,1,0);
//		glEnd();
//		
//		//hinten
//		glBegin(GL_TRIANGLES);
//		glColor3f(0.8f,0.8f,0.8f);
//		glVertex3f(1,-1,-1);
//		glVertex3f(-1,-1,-1);
//		glVertex3f(0,1,0);
//		glEnd();
//		
//		//links
//		glBegin(GL_TRIANGLES);
//		glColor3f(0.6f,0.6f,0.6f);
//		glVertex3f(-1,-1,-1);
//		glVertex3f(-1,-1,1);
//		glVertex3f(0,1,0);
//		glEnd();
//				
		i++;
		
		step+=diffTime;
		
		cubescale=Math.abs(Math.sin(step/200f)*1.2)+0.2;
	}
}
