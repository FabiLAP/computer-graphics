#version 150

varying vec2 uv;
uniform mat4 mvpMatrix;
uniform mat4 projectionMatrix;
varying vec3 normal;
varying vec4 vertex;
varying vec3 licht;



void main(void) {
	uv=gl_MultiTexCoord0.st;
	gl_Position = mvpMatrix * projectionMatrix *  gl_Vertex;
	normal=vec3(transpose(inverse(mvpMatrix)) * vec4(gl_Normal,0));
	vertex = mvpMatrix*gl_Vertex;
	licht = licht;

}