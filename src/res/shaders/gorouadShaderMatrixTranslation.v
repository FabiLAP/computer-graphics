varying vec4 color;
uniform mat4 mvpMatrix;
varying vec2 uv;

void main(void) {
	uv=gl_MultiTexCoord0.st;
	color = gl_Color; 
	gl_Position = mvpMatrix * gl_Vertex;
}