#version 120
//phong

varying vec2 uv;
uniform sampler2D s;
varying vec3 normal;
varying vec4 vertex;
uniform vec3 licht;
uniform mat4 mvpMatrix;
uniform mat4 projectionMatrix;

void main (void) { 

	//l
	vec3 l = vec3(2,2,0);//licht;
	l = normalize(l);
	
	vec3 normale = normalize(normal);
	
	//cam
	vec3 campos = vec3(0,0,0);
	vec3 cam =  campos-vertex.xyz;
	cam = normalize(cam);
	
	float ambient=20;
	
	//diffuse = I*(L*N)
	float diffuse = dot(normale, l);
	
	//R = 2*(L*N)*N-L
	vec3 r = 2*dot(l,normale)*normale-l;
	
	//specular = I*(R*V)*(R*V)
	float specular=dot(r, cam)*dot(r, cam);
	
	float phong = ambient+diffuse+specular;
	gl_FragColor = vec4(1,1,1,1);//(texture2D(s,uv)*phong);
}