varying vec2 uv;
varying vec3 normal;
varying vec3 vertex;

void main(void) {
uv=gl_MultiTexCoord0.xy;
normal = gl_NormalMatrix*gl_Normal;
vertex = (gl_ModelViewMatrix * gl_Vertex).xyz;
gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}