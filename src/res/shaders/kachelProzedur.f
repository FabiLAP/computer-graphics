#version 120
//prozedur

varying vec2 uv;
uniform sampler2D s;
varying vec3 vertex;

vec4 meineProzedur(float u, float v){
	return vec4(u,v,u+v,1);
}

vec4 meineProzedur2(float u, float v){
	if(mod(u*11 , 2) <= 1 || mod(v*11 , 2) <= 1){
			return vec4(0,0,0,1);
		}
		else{
			return vec4(1,1,1,1);		
		}
}

void main (void) { 
	
	gl_FragColor = meineProzedur2(uv.x, uv.y);
}