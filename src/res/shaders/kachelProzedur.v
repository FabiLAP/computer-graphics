varying vec2 uv;
varying vec3 normal;
varying vec3 vertex;
uniform vec3 licht;
varying vec4 color;
uniform mat4 mvpMatrix;

void main(void) {
	uv=gl_MultiTexCoord0.st;
	gl_Position = mvpMatrix * gl_Vertex; 
	color = gl_Color;
	normal=gl_NormalMatrix * gl_Normal;
}