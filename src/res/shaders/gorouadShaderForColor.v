varying vec3 vertex;
varying vec4 color;
uniform mat4 mvpMatrix;


void main(void) {
	color = gl_Color; 
	gl_Position = mvpMatrix * gl_Vertex; 
}