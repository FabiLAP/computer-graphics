import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.Display;

import lenz.opengl.AbstractSimpleBase;

public class CopyOfExample extends AbstractSimpleBase {
	
	int i =0;
	int j = 80;

	public static void main(String[] args) {
		new CopyOfExample().start();
	}

	@Override
	protected void initOpenGL() {
		glMatrixMode(GL_PROJECTION);
		glOrtho(-1, 1, -1, 1, 0, 1);
		glMatrixMode(GL_MODELVIEW);

		glShadeModel(GL_FLAT);
	}

	@Override
	protected void render() {
		glClear(GL_COLOR_BUFFER_BIT);
		glLoadIdentity();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0, 0, 0);
		glColor3f(0.5f, 0.5f, 0.5f);
		glVertex2i(0, 0); //definiert eine Ecke/Punkt //2i - Anzahl Parameter + Type
		for (int l=0;l<360;l++){
			glColor3f(0.5f, 0.5f, 0.5f);
			glVertex2d(Math.sin(l)*0.01*i, Math.cos(l)*0.01*i); //definiert eine Ecke/Punkt //2i - Anzahl Parameter + Type
		}

		glEnd();
		
		glLoadIdentity();
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(1, 1, 1);
		
		glVertex2i(0, 0); //definiert eine Ecke/Punkt //2i - Anzahl Parameter + Type
		for (int l=0;l<360;l++){
	
			glVertex2d(Math.sin(l)*0.01*i, Math.cos(l)*0.01*i); //definiert eine Ecke/Punkt //2i - Anzahl Parameter + Type
		}

		glEnd();
		
		
		if(i==160){
			i=0;
		}
	
		
//		glBegin(GL_LINES);
//		glColor3f(1, 1, 1);
//		glVertex2i(100, 100);
//		glVertex2i(150, 288);
//		glEnd();
//		
//		
//		
//		glTranslated(309,309.,0.);
//		glRotated(i, 0, 0, 1);		
//		glBegin(GL_TRIANGLES);
//		glColor3f(1, 0, 1);				
//		glVertex2i(0, 0);
//		glVertex2i(100, 100);
//		glVertex2i(0, 100);		
//		glEnd();
//		
//		glLoadIdentity();
//
//		glTranslated(10+i,200.,0.);
//		glBegin(GL_POLYGON);
//		glColor3f(0, 1, 1);
//		glVertex2i(100, 300); //definiert eine Ecke/Punkt //2i - Anzahl Parameter + Type
//		glVertex2i(100, 359);
//		glVertex2i(120, 360);
//		glVertex2i(110, 380);
//		glVertex2i(170, 200);
//		glVertex2i(110, 220);
//		glVertex2i(145, 131);	
////		glTranslated(200.,200.,200.);
//		glEnd();
//		
//		glLoadIdentity();
//		
//		
//		glScaled(i, i, 0);
//		glBegin(GL_TRIANGLES);
//		glColor3f(1, 1, 1);				
//		glVertex2i(0, 0);
//		glVertex2i(1, 1);
//		glVertex2i(0, 1);		
//		glEnd();

		i++;
		

		
		
	}
}
