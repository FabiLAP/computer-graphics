import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;

import lenz.opengl.AbstractSimpleBase;

public class Example_Ue2 extends AbstractSimpleBase {
	
	int i =0;
	long time = Sys.getTime();
	long step=0;
	double cubescale = 0;

	public static void main(String[] args) {
		new Example_Ue2().start();
	}

	@Override
	protected void initOpenGL() {
		glMatrixMode(GL_PROJECTION);
		glFrustum(-1, 1, -1, 1, 1, 100);
		glMatrixMode(GL_MODELVIEW);
		glShadeModel(GL_FLAT);
		
		//schalten fl�chen hinten uswe waus
		
		glEnable(GL_CULL_FACE);
	}

	@Override
	protected void render() {
		
		long diffTime = Sys.getTime()-time;
		time = Sys.getTime();
			
		glClear(GL_COLOR_BUFFER_BIT);
		glLoadIdentity();
//		
//		glTranslated(0,0,-2);
//		glBegin(GL_LINES);
//		glColor3f(1,1,1);
//		glVertex3f(-1,0,0);
//		glVertex3f(1,0,0);
//		glEnd();
//		
//		glLoadIdentity();
//		glTranslated(0,1,-5);
//		glBegin(GL_TRIANGLES);
//		glColor3f(1,1,1);
//		glVertex3f(-1,0,0);
//		glVertex3f(1,0,0);
//		glVertex3f(0,1,0);
//		glEnd();
		
		//################################### cube
		
		glLoadIdentity();
		glTranslated(0,2,-5);
		glScaled(cubescale, cubescale, cubescale);
		glRotated(step/10f, 1, 1, 0);
		glBegin(GL_QUADS);
		
		//vorn
		glColor3f(0.9f,0.9f,0.9f);
		glVertex3f(-1,-1,1);
		glVertex3f(1,-1,1);
		glVertex3f(1,1,1);
		glVertex3f(-1,1,1);
		
		//hinten
		glColor3f(0.8f,0.8f,0.8f);
		glVertex3f(1,-1,-1);
		glVertex3f(-1,-1,-1);
		glVertex3f(-1,1,-1);
		glVertex3f(1,1,-1);
		
		//rechts
		glColor3f(0.7f,0.7f,0.7f);
		glVertex3f(1,-1,1);
		glVertex3f(1,-1,-1);
		glVertex3f(1,1,-1);
		glVertex3f(1,1,1);
		
		//links
		glColor3f(0.6f,0.6f,0.6f);
		glVertex3f(-1,-1,1);
		glVertex3f(-1,1,1);
		glVertex3f(-1,1,-1);
		glVertex3f(-1,-1,-1);
		
		//oben
		glColor3f(0.5f,0.5f,0.5f);
		glVertex3f(-1,1,1);
		glVertex3f(1,1,1);
		glVertex3f(1,1,-1);
		glVertex3f(-1,1,-1);
		
		//unten
		glColor3f(0.4f,0.4f,0.4f);
		glVertex3f(1,-1,1);
		glVertex3f(-1,-1,1);
		glVertex3f(-1,-1,-1);
		glVertex3f(1,-1,-1);
		glEnd();
		
		//###############################################
		
		glLoadIdentity();
		glTranslated(0,-2,-5);
		glRotated(step/5f, 1, 0, 0);
		glRotated(step/5f, 0, 1, 0);
		glBegin(GL_QUADS);		
		
		//unten
		glColor3f(0.4f,0.4f,0.4f);
		glVertex3f(1,-1,1);
		glVertex3f(-1,-1,1);
		glVertex3f(-1,-1,-1);
		glVertex3f(1,-1,-1);
		glEnd();
		
		//vorn
		glBegin(GL_TRIANGLES);
		glColor3f(0.9f,0.9f,0.9f);
		glVertex3f(-1,-1,1);
		glVertex3f(1,-1,1);
		glVertex3f(0,1,0);
		glEnd();
		
		//rechts
		glBegin(GL_TRIANGLES);
		glColor3f(0.7f,0.7f,0.7f);
		glVertex3f(1,-1,1);
		glVertex3f(1,-1,-1);
		glVertex3f(0,1,0);
		glEnd();
		
		//hinten
		glBegin(GL_TRIANGLES);
		glColor3f(0.8f,0.8f,0.8f);
		glVertex3f(1,-1,-1);
		glVertex3f(-1,-1,-1);
		glVertex3f(0,1,0);
		glEnd();
		
		//links
		glBegin(GL_TRIANGLES);
		glColor3f(0.6f,0.6f,0.6f);
		glVertex3f(-1,-1,-1);
		glVertex3f(-1,-1,1);
		glVertex3f(0,1,0);
		glEnd();
				
		i++;
		
		step+=diffTime;
		
		cubescale=Math.abs(Math.sin(step/200f)*1.2)+0.2;
	}
}
